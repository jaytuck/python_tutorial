import requests
import xml.etree.ElementTree as ET
import re
import csv


# sitemap = requests.get('https://www.cdu.edu.au/sitemap.xml?page=1')

linda_regex = r'.*linda.*'
timor_regex = r'.*timor.*'
webex_regex = r'.*webex.*'

output_file = 'found_pages.csv'

tree = ET.parse('cdu_sitemap_20201112.xml').getroot()

def search_site():
    with open(output_file, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['page_url'])

        for counter, page_url in enumerate(tree.iter('loc')):
            if counter % 50 == 0:
                print(f"up to page {page_url.text}")
            page = requests.get(page_url.text)
            if re.findall(webex_regex, page.text, re.IGNORECASE):
                print(page.url, "Webex match")
                writer.writerow([page_url.text])
#            if re.findall(timor_regex, page.text, re.IGNORECASE):
#                print(page.url, "Timor match")
#                print(re.findall(timor_regex, page.text, re.IGNORECASE))


zoom_regex = r'.*zoom.*'
def zoom_search():
    for counter, page_url in enumerate(tree.iter('loc')):
        if 'itms' in page_url.text:
            print(f"up to page {page_url.text}")
            page = requests.get(page_url.text)
            print(re.findall(zoom_regex, page.text, re.IGNORECASE))



if __name__ == "__main__":
    search_site()


