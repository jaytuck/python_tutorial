import csv

# Function to merge a row with the other list
def matchrow(row, other_list):
    new_row = [row[0], row[1], '']
    # For each row in the other list
    for other_row in other_list:
        # check if the name matches to the row
        if other_row[0] == row[1] or other_row[0][1:] == row[1]:
            print(f"found matching row for computer {other_row[0]}")
            new_row[2] += other_row[1] + ', '
    return(new_row)


# Read csv's
with open('pc_list.csv', newline='') as file:
    csv_reader = csv.reader(file)
    master_list = list(csv_reader)

print(f"pc list read. Length is {len(master_list)}")
print(f"top few rows", master_list[:5])

with open('user_affinity_list.csv', newline='') as file:
    csv_reader = csv.reader(file)
    user_affinity_list = list(csv_reader)

print(f"user list read. Length is {len(user_affinity_list)}")
print(f"top few rows", user_affinity_list[:5])

new_master_list = []

# Run through each row of master csv
for row in master_list[:99]:
    ## For each row, find any rows on the user_affinity csv that match
    new_master_list.append(matchrow(row, user_affinity_list))

for row in new_master_list[:99]:
    print(row)

# Export the master csv again
with open('output_file.csv', 'w') as file:
    writer = csv.writer(file)
    writer.writerows(new_master_list)
